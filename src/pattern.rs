use std::convert::From;

pub enum Pattern {
    Name(RawPattern),
    FilesLoose(RawPattern),
}

pub struct RawPattern(String);

impl RawPattern {
    pub fn contains_smart_cased(&self, sample: &str) -> bool {
        if self.has_upper() {
            sample.contains(&self.0)
        } else {
            sample.to_ascii_lowercase().contains(&self.0)
        }
    }

    fn has_upper(&self) -> bool {
        for c in self.0.chars() {
            if c.is_uppercase() {
                return true;
            }
        }
        false
    }
}

impl From<String> for RawPattern {
    fn from(s: String) -> Self {
        Self(s)
    }
}
