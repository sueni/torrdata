use crate::pattern::Pattern;
use std::path::PathBuf;

pub enum Chunk {
    TorrentPath,
    Name,
    Comment,
    Announce,
    Size,
    Privacy,
    FilesCount,
    Files,
    Subdirectories,
    Extra(String),
}

pub struct Args {
    pub chunks: Vec<Chunk>,
    pub chunk_delim: String,
    pub torr_delim: String,
    pub patterns: Vec<Pattern>,
    pub torr_files: Vec<PathBuf>,
}

pub const HELP: &str = "
USAGE:
  torrdata [OPTIONS] torrent-file(s)

DISPLAY SECTIONS:
  -c  comment
  -f  files count
  -l  files list
  -i  subdirectories list
  -n  torrent name
  -p  privacy
  -s  size
  -t  torrent file path

OPTIONS:
  -N <PATTERN>    match name against substring
  -L <PATTERN>    match filenames against substring
  -d <DELIMITER>  delimiter within a torrent (default: NEWLINE)
  -D <DELIMITER>  delimiter between torrents (default: NEWLINE)
  -e <EXTRA>      chunk of text to put between sections";

pub fn parse_args() -> Result<Args, lexopt::Error> {
    use lexopt::prelude::*;

    let mut torr_files = Vec::new();
    let mut chunks = Vec::new();
    let mut chunk_delim = "\n".to_string();
    let mut torr_delim = "\n".to_string();
    let mut patterns = Vec::new();
    let mut parser = lexopt::Parser::from_env();

    while let Some(arg) = parser.next()? {
        match arg {
            Short('t') => chunks.push(Chunk::TorrentPath),
            Short('n') => chunks.push(Chunk::Name),
            Short('c') => chunks.push(Chunk::Comment),
            Short('a') => chunks.push(Chunk::Announce),
            Short('s') => chunks.push(Chunk::Size),
            Short('p') => chunks.push(Chunk::Privacy),
            Short('f') => chunks.push(Chunk::FilesCount),
            Short('l') => chunks.push(Chunk::Files),
            Short('i') => chunks.push(Chunk::Subdirectories),
            Short('d') => chunk_delim = parser.value()?.parse()?,
            Short('D') => torr_delim = parser.value()?.parse()?,
            Short('e') => {
                let string = parser.value()?.parse()?;
                chunks.push(Chunk::Extra(string));
            }
            Short('N') => {
                let pattern: String = parser.value()?.parse()?;
                patterns.push(Pattern::Name(pattern.into()));
            }
            Short('L') => {
                let pattern: String = parser.value()?.parse()?;
                patterns.push(Pattern::FilesLoose(pattern.into()));
            }
            Value(val) => {
                let path = PathBuf::from(val);
                if !path.exists() {
                    eprintln!("File not exists: {}", path.display());
                }
                torr_files.push(path);
            }
            _ => return Err(arg.unexpected()),
        }
    }

    if torr_files.is_empty() {
        return Err("missing torrent file(s)".into());
    }

    if chunks.is_empty() {
        return Err("need at least one flag to display".into());
    }

    Ok(Args {
        chunks,
        chunk_delim,
        torr_delim,
        patterns,
        torr_files,
    })
}
