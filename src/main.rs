use number_prefix::NumberPrefix;
use std::collections::BTreeSet;
use std::io::{self, Read, Write};
use std::os::unix::ffi::OsStrExt;
use std::path::Path;

mod cli;
use cli::Args;
use cli::Chunk;

mod schema;
use schema::Torrent;

mod pattern;
use crate::pattern::Pattern;

fn read_file(path: &Path, buf: &mut Vec<u8>) -> io::Result<()> {
    let mut file = std::fs::File::open(path)?;
    buf.clear();
    file.read_to_end(buf)?;
    Ok(())
}

fn maybe_delim(limit: usize, idx: usize, delim: &str, out: &mut impl Write) -> io::Result<()> {
    if idx + 1 < limit {
        out.write_all(delim.as_bytes())?;
    }
    Ok(())
}

fn reveal(tp: &Path, torrent: Torrent, args: &Args) -> io::Result<()> {
    let mut out = io::stdout().lock();

    for (ci, chunk) in args.chunks.iter().enumerate() {
        match chunk {
            Chunk::TorrentPath => {
                out.write_all(tp.as_os_str().as_bytes())?;
            }
            Chunk::Name => {
                if let Some(name) = torrent.name() {
                    out.write_all(name.as_bytes())?;
                }
            }
            Chunk::Comment => {
                if let Some(comment) = &torrent.comment {
                    out.write_all(comment.as_bytes())?;
                }
            }
            Chunk::Announce => {
                if let Some(announce) = &torrent.announce {
                    out.write_all(announce.as_bytes())?;
                }
            }
            Chunk::Size => {
                let size = match NumberPrefix::decimal(torrent.total_size() as f64) {
                    NumberPrefix::Standalone(bytes) => format!("{} bytes", bytes),
                    NumberPrefix::Prefixed(prefix, n) => format!("{:.1} {}B", n, prefix),
                };
                out.write_all(size.as_bytes())?;
            }
            Chunk::Privacy => {
                let private = match torrent.private() {
                    None => "public",
                    Some(_) => "private",
                };
                out.write_all(private.as_bytes())?;
            }
            Chunk::FilesCount => out.write_all(torrent.files_count().to_string().as_bytes())?,
            Chunk::Files => {
                if let Some(files) = torrent.files() {
                    for (fi, file) in files.iter().enumerate() {
                        write!(out, "{} ", fi + 1)?;
                        for (pi, piece) in file.path_pieces().iter().enumerate() {
                            out.write_all(piece)?;
                            maybe_delim(file.path_pieces().len(), pi, "/", &mut out)?;
                        }
                        maybe_delim(files.len(), fi, &args.chunk_delim, &mut out)?;
                    }
                }
            }
            Chunk::Subdirectories => {
                if let Some(files) = torrent.files() {
                    let mut dirs = BTreeSet::new();
                    for file in files {
                        if let Some(parent) = file.parent_dir() {
                            dirs.insert(parent);
                        }
                    }
                    for (i, dir) in dirs.iter().enumerate() {
                        out.write_all(dir.as_os_str().as_bytes())?;
                        maybe_delim(dirs.len(), i, &args.chunk_delim, &mut out)?;
                    }
                }
            }
            Chunk::Extra(string) => out.write_all(string.as_bytes())?,
        }
        maybe_delim(args.chunks.len(), ci, &args.chunk_delim, &mut out)?;
    }
    out.write_all(args.torr_delim.as_bytes())?;
    Ok(())
}

fn run(args: &cli::Args) -> io::Result<()> {
    let mut buffer: Vec<u8> = Vec::new();

    for tf in &args.torr_files {
        match read_file(tf, &mut buffer) {
            Err(e) => eprintln!("{}: {}", tf.display(), e),
            Ok(_) => match Torrent::from_buf(&buffer) {
                Ok(torrent) => {
                    if test(&torrent, &args.patterns) {
                        reveal(tf, torrent, args)?
                    }
                }
                Err(e) => eprintln!("{}: {}", tf.display(), e),
            },
        }
    }
    Ok(())
}

fn test(torrent: &Torrent, patterns: &[Pattern]) -> bool {
    if patterns.is_empty() {
        return true;
    }

    for pattern in patterns {
        match pattern {
            Pattern::Name(rp) => {
                if let Some(name) = torrent.name() {
                    if rp.contains_smart_cased(name) {
                        return true;
                    };
                }
            }
            Pattern::FilesLoose(rp) => {
                if let Some(files) = torrent.files() {
                    for file in files {
                        for piece in file.path_pieces() {
                            if rp.contains_smart_cased(&String::from_utf8_lossy(piece)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }
    false
}

fn main() {
    let args = match cli::parse_args() {
        Ok(args) => args,
        Err(e) => {
            eprintln!("ERROR: {}\n{}", e, cli::HELP);
            std::process::exit(1);
        }
    };

    if let Err(e) = run(&args) {
        eprintln!("{}", e);
        std::process::exit(1);
    }
}
