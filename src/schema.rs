use serde_bytes::ByteBuf;
use serde_derive::{Deserialize, Serialize};
use std::error::Error;
use std::ffi::OsStr;
use std::os::unix::ffi::OsStrExt;
use std::path::PathBuf;

#[derive(Default, Deserialize)]
pub struct Torrent {
    info: Info,
    pub comment: Option<String>,
    pub announce: Option<String>,
}

impl Torrent {
    pub fn from_buf(buf: &[u8]) -> Result<Self, Box<dyn Error>> {
        Ok(serde_bencode::de::from_bytes(buf)?)
    }

    fn info(&self) -> &Info {
        &self.info
    }

    pub fn name(&self) -> &Option<String> {
        &self.info().name
    }

    pub fn private(&self) -> Option<u8> {
        self.info().private
    }

    pub fn total_size(&self) -> usize {
        match &self.files() {
            None => self.info.length.unwrap_or_default(),
            Some(files) => files.iter().fold(0, |acc, f| acc + f.length),
        }
    }

    pub fn files_count(&self) -> usize {
        match self.files() {
            Some(f) => f.len(),
            None => 1,
        }
    }

    pub fn files(&self) -> &Option<Vec<File>> {
        &self.info().files
    }
}

#[derive(Default, Serialize, Deserialize)]
pub struct Info {
    pub name: Option<String>,
    pub private: Option<u8>,
    pub files: Option<Vec<File>>,
    pub length: Option<usize>,
}

#[derive(Default, Serialize, Deserialize)]
pub struct File {
    #[serde(rename = "path")]
    path_pieces: Vec<ByteBuf>,
    length: usize,
}

impl File {
    pub fn path_pieces(&self) -> &[ByteBuf] {
        &self.path_pieces
    }

    pub fn parent_dir(&self) -> Option<PathBuf> {
        let plen = self.path_pieces.len();
        if plen > 1 {
            let mut dir = PathBuf::from("");
            self.path_pieces[..plen - 1]
                .iter()
                .rev()
                .for_each(|p| dir.push(OsStr::from_bytes(p)));
            return Some(dir);
        }
        None
    }
}
